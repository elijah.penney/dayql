const axios = require('axios');

/**
 * Class representing a DayQL instance
 */
class DayQL {

  /**
   * Create an instance of DayQL
   * @param {Object} args - Information for initializing DayQL
   * @param {string} args.baseURL - URL for standard GraphQL requests
   * @param {Object} args.headers - Headers to append to every request
   * @param {Object} args.defaultVariables - Variables that should be attached to every request
   */
  constructor({ baseURL = '', headers = {}, defaultVariables = {} } = {}) {
    this.axios = axios.create({
      baseURL,
      headers,
    });
    this.defaultVariables = defaultVariables;
  }

  /**
   * Query the GraphQL Server
   * @param {Object} args - Information to construct a query
   * @param {(string | Object)} args.query - The query to send to GraphQL
   * @param {Object} args.variables - Variables to augment the query
   */
  query({ query = '', variables = {} } = {}) {
    return this.axios.post('', {
      query,
      variables: { ...this.defaultVariables, ...variables },
    });
  }

  /**
   * Mutate the GraphQL Server
   * @param {Object} args - Information to construct a query
   * @param {(string | Object)} args.mutation - The query to send to GraphQL
   * @param {Object} args.variables - Variables to augment the query
   */
  mutate({ mutation = '', variables = {} } = {}) {
    return this.axios.post('', {
      mutation,
      variables: { ...this.defaultVariables, ...variables },
    });
  }

}

module.exports = DayQL;
